package by.itacademy.lesson5.atm;

public class Main {
    public static void main(String[] args) {
        BelInvestATM atm = new BelInvestATM(3, 2, 3);
        System.out.println(atm.show());
        atm.debit(100);
        atm.debit(100);
        atm.debit(50);
        atm.debit(50);
        atm.debit(20);
        System.out.println(atm.show());
        atm.credit(500);
        System.out.println(atm.show());
        System.out.println(atm.show());
        System.out.println(atm.name() + "," + atm.manufacture());
        System.out.println(atm);

    }
}
