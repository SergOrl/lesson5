package by.itacademy.lesson5.atm;

public interface Withdraw {
    boolean credit(int amount);
}
